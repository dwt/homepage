
title: Function Call vs. Message Send
---
author: Martin Häcker
---
pub_date: 2008-04-26
---
categories: fnord, code
---
body:

Oder: Wieso mich die Lisp Syntax so stört.

In meiner Freizeit beschäftige ich mich gerade mit dem Buch [Practical Common Lisp](http://gigamonkeys.com/book/). 

Das Buch gefällt mir auch ganz gut - vielleicht abgesehen davon das er 'loop etwas zu häufig verwendet - ohne es zu erklären - was den Code gerade am Anfang reichlich magisch macht.

Aber: Worum es mir eigentlich geht, ist die Syntax von Lips. Nicht die Klammern, nicht die Einrückung, nicht die Art wie man es schreibt, sondern schlicht die Reihenfolge.

Weil: in Lisp ist erst einmal alles auf Funktionen abgebildet. Und das hat Vorteile aber eben auch einen gravierenden Nachteil: Jede Funktion muss immer als erstes in einem Funktionsaufruf Tupel stehen.

```lisp
(funktion argument-eins argument-zwei)
```

Das ist ein Problem, weil man bei verschachtelten Funktionsaufrufen (auch schon bei nur sehr wenigen Ebenen) sehr schnell den Überblick verliert welcher Funktionsaufruf zu welchem argument gehört.

```lisp
(funktion (funktion-zwei argument-eins (funktion-drei argument-eins argument-zwei)) argument-zwei)
```

Das kann man jetzt natürlich auch als Feature sehen, weil man gezwungen wird seinen Code vernünftig einzurücken und keine zu langen Funktionen zu schreiben

```lisp
(funktion (funktion-zwei argument-eins
					 (funktion-drei argument-eins argument-zwei)) 
		argument-zwei)
```

Das hilft, aber eben nur so weit. Vor allem weil man beim Lesen von Code immer eine interne herumdrehung machen muss um den Code korrekt zu lesen.

```lisp
(defparameter a-list (list 1 2 3 4 5))

(car (cdr (cdr (cdr a-list))))
```

Soll (wenn (ich) (mich nicht verschrieben) habe) 5 ergeben.

Aber man muss es von innen nach aussen lesen, damit man es richtig versteht.

Und das erfordert jede menge Geistige Kapazität, die besser beim finden von guten Namen für Variablen und Funktionen aufgehoben ist. Viel besser.

Jetzt zum Message-Passing - das ist zwar fundamental "weniger mächtig" als der Funktionsaufruf, weil man zum Beispiel generische Funktionen nicht oder nicht so gut implementieren kann, ABER: man kann viel besser mit dem resultat eines vorherigen Aufrufes das nächste tun.

Weil die Reihenfolge in der die Sachen ausgeführt werden die gleiche ist in der die Sachen auf dem Bildschirm stehen.

Und das ist eine ganze Menge besser les- und versteh-bar als die Funktion es jemals sein kann.

Das war es was [Alan Kay](http://de.wikipedia.org/wiki/Alan_Kay) meinte, als er sagt das das wichtigste an [Smalltalk](http://de.wikipedia.org/wiki/Smalltalk_(Programmiersprache)) nicht die Objektorientierung, sondern das [Message Passing](http://c2.com/cgi/wiki?AlanKayOnMessaging) gewesen ist.

