
title: Experimentalphysik und Programmieren
---
author: Martin Häcker
---
pub_date: 2008-05-10
---
categories: code
---
body:

Das sind an sich identische Tätigkeiten - hat mir mein Vater heute klargemacht.

Klar, er ist ein alter Physiker und trauert immer noch ein wenig der Tatsache nach das ich keiner geworden bin.

Oder auch nicht, denn: Ein guter Experimentalphysiker arbeitet so, dass er in einer Versuchsreihe in jedem Schritt immer nur eine Variable verändert um methodisch vorzugehen. Entweder nimmt er eine (wirklich nur eine) Änderung an seinen Messinstrumenten, oder an dem Experiment vor.

Dazu dann natürlich noch einiges an Intuition welche Veränderung sinnvoll ist - experimentelle Physik ist eben auch eine Kunst, genau wie das Programmieren.

Ach ja, und natürlich schreibt er ein Protokoll um hinterher nachvollziehen zu können, was er eigentlich getan hat.

Beim Programmieren kann man diese Techniken genau so wiederfinden. Die Messinstrumente sind in der Regel [UnitTests](http://c2.com/cgi/wiki?UnitTest) oder etwas äquivalentes (wobei UnitTests besser geeignet sind, da sie exakter messen können) und damit wird eine API dann abgeklopft.

Jetzt kommt bei uns Programmierern aber noch eine komplette Dimension dazu. Denn während ein Experimentalphysiker die Welt um ihn herum so hinnehmen muss wie sie ist, können wir Programmierer den Gegenstand unserer Tests beliebig verändern. Nur unser Phantasie schränkt uns ein.

Aber es läuft genauso: Ein Messinstrument, ein Stück Weltveränderung, ad infinitum.

Das letzte fehlende Detail ist das Logbuch: Das sind die schon geschriebenen Tests, die dokumentieren was wir schon herausgefunden haben.

Ich war wirklich überrascht wie weit die analogie trägt. Und auch die Implikation gefällt mir als UnitTest-Fan natürlich sehr. Und natürlich auch der Fakt das wir als Informatiker 'wieder mal' mehr können als die Physiker.

:-)

