
title: Sauberes Programmieren
---
author: Martin Häcker
---
pub_date: 2008-03-04
---
categories: fnord, code
---
body:

(zu [Return to Innocence von Enigma](http://musicbrainz.org/track/127f318a-28d3-49be-b5f6-be55f4d596b9.html))

Es ist wirklich erstaunlich wie viel schwieriger es ist saubere Programme zu schreiben wenn mehr Leute an der Konstruktion beteiligt sind.

Nehmen wir zum Beispiel drei Leute. Einer hat vorher viel C++ progirammiert. Ganz selbstverständlich ist für ihn das Paradigma geworden, das man Accessoren `-getVar` und Mutatoren `-setVar:aValue` nennt.

Und schon mischt sich im System der Namens-Salat. Und das ist noch keine große Sache. Von sich aus besteht keinerlei Einheitlichkeit, wie wer seine Variablen und Funktionen nennt. Wie z.B. heißt eine Klassenmethode zum erstellen eines Objekts? `-classname:instanceVarName with:anotherInstanceVarname` oder `-intentionWithNamedThing:aTypeDescription withAnotherNamedThing:aTypeDescription`

Wie geht man mit Prefixen um? Was will man mit Instanzvariablen tun? Wann verwendet man (sinnvollerweise Accessoren und wann direkten Zugriff auf Variablen?

Die Summe dieser kleinen Entscheidungen macht so viel von der ([olfaktorischen?](http://c2.com/xp/CodeSmell.html)) Sauberheit eines Programms aus, das man sich (eigentlich) nicht leisten kann sie dem Zufall zu überlassen.

Aber der Overhead das alles vorher festzulegen oder auch nur zu kommunizieren ist gewaltig. Vor allem wenn man weder Refaktoring-Werkzeuge hat die einem die Änderungen erleichtern, noch Unit-Tests die einem die Änderungen absichern, noch Reviews / Pair-Programming die einen zu der Entscheidung helfen was geändert werden soll.

Was tun?

Erleichterung finde ich in der Tatsache, das tatsächlich viele kleinen Entscheidungen kaum oder keine Rolle spielen. Wo die Klammern gesetzt werden, oder wie weit eingerückt wird ist egal. Man überließt diese Unterschiede einfach.

Dazu kommt die Aussage aus dem [Big Ball of Mud](http://www.laputan.org/mud/mud.html) Paper: Ein Dreckball mag besser sein als gar keine Software.

Ist Sauberkeit also doch überschätzt?

