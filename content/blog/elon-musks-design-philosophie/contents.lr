title: Elon Musks Design Philosophie
---
body:

Beim lesen [dieses Interview mit Elon Musk](https://everydayastronaut.com/starbase-tour-and-interview-with-elon-musk/) ([hier auch als Video](https://www.youtube.com/watch?v=t705r8ICkRw)) hatte ich wirklich Spaß.

Diese Dinge sind nichts neues - aber mir ist klar geworden dass ich in der Vergangenheit viel zu Wenig wert darauf gelegt habe Requirements als Dumm zu identifizieren und Los zu werden, sowie Prozesse zu löschen. Autsch. Mea Culpa - ich gelobe Besserung.

Hier vor allem für mich selbst archiviert:

## Musk’s Engineering Philosophy:

Musk overviewed his five step engineering process, which must be completed in order:

1. Make the requirements less dumb. The requirements are definitely dumb; it does not matter who gave them to you. He notes that it’s particularly dangerous if someone who is smart gives them the requirements, as one may not question the requirements enough. “Everyone’s wrong. No matter who you are, everyone is wrong some of the time.” He further notes that “all designs are wrong, it’s just a matter of how wrong.”
2. Try very hard to delete the part or process. If parts are not being added back into the design at least 10% of the time, not enough parts are being deleted. Musk noted that the bias tends to be very strongly toward “let’s add this part or process step in case we need it.” Additionally, each required part and process must come from a name, not a department, as a department cannot be asked why a requirement exists, but a person can.
3. Simplify and optimize the design. This is step three as the most common error of a smart engineer is to optimize something that should not exist.
4. Accelerate cycle time. Musk states “you’re moving too slowly, go faster! But don’t go faster until you’ve worked on the other three things first.”
5. Automate. An important part of this is to remove in-process testing after the problems have been diagnosed; if a product is reaching the end of a production line with a high acceptance rate, there is no need for in-process testing.
Additionally, Musk restated that he believes everyone should be a chief engineer. Engineers need to understand the system at a high level to understand when they are making a bad optimization. As an example, Musk noted that an order of magnitude more time has been spent reducing engine mass than reducing residual propellant, despite both being equally as important.


---
categories: code
---
pub_date: 2021-10-21
