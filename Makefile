.PHONY:
deploy:
	lektor build
	# neeed to create the CNAME file so github knows what domain to use
	echo xn--hcker-gra.net > dist/CNAME
	(cd dist; git add . ; git commit -m "buid $(date)"; git push)

.PHONY:
serve:
	LEKTOR_DEV=1 lektor server
