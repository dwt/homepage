title: Wie nutze ich die Shell effektiv
---
body:

![Häcker bei der Arbeit…](hacker.jpeg)
Bei meiner Arbeit ist mir aufgefallen, dass viele Entwickler bei uns die Shell viel effektiver nutzen könnten, um ihre Arbeit besser und schneller zu machen. Da ich die Shell sehr viel nutze, habe ich dazu ein paar Ideen. 😇 Auf der anderen Seite gibt es immer wieder ein Nugget, das ich selbst nicht kenne – und das möchte ich natürlich auch lernen.  

In diesem Sinne: Ich freue mich über Tips, lasst uns gerne daraus einen Austausch machen, wie man seine Shell effektiv einsetzt.

Damit verständlich wird wie ich meine Shell verwende ist es wichtig zu verstehen dass es verschiedene Arten gibt einen Mac effektiv zu nutzen. Ich kategorisiere das für mich grob so:

* Fensterbasiert: Viele Fenster, oft klein, die sich gegenseitig teilweise verdecken. Spezialisierte Apps anstatt Monolithen. Terminal, Editor, Git GUI, Datenbank-GUI und ein Dokumentationsbrowser, anstatt einer IDE die alles inkludiert.
* Screen/Tmux Style: Auch viele Fenster, aber ohne das diese sich überlappen. Viel Full-Screen und oft ein Window Manger oder Terminal das sicherstellt das Fenster garantiert nebeneinander sind. IDEs arbeiten in der Regel so.
* Quake Shell-Style: Im wesentlichen irgend ein anderer Stil, aber mit einem globalen Shortcut, der jederzeit eine Shell in den Vordergrund bringt oder wieder verschwinden lässt - egal wo man gerade ist.
* Mehrere Desktops: Separation von Apps über mehrere Desktops oder Workspaces.

Ich bin ganz klar tief in dem Fensterbasierten Workflow verortet. Ich kenne natürlich auch die anderen Workflows, aber sie funktionieren für mich persönlich einfach nicht so gut. Ich finde überlappende Fenster wirklich großartig, weil ich damit auf einem sehr viel kleineren Bildschirm (14-Zoll-Notebook) genauso produktiv sein kann, wie auf einem sehr viel größeren Bildschirm. 

Ich hoffe sehr, dass die Apple-Vision bald so gut funktioniert und so leicht wird, wie ich es mir wünsche. Dann kann ich es mir meine Fenster fei um mich herum anordnen. Aber bis dahin bleibe ich bei überlappenden Fenstern.
---
pub_date: 2024-05-24
---
categories: code
