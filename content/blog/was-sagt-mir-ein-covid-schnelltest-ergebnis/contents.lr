title: Was sagt mir ein Covid-Schnelltest Ergebnis?
---
body:

Inspiriert von [diesem Video von 3Blue1Brown](https://www.youtube.com/watch?v=lG4VkPoG3ko), hat sich mein Verständnis wie man über Covid-Tests nachdenkt, grundsätzlich gewandelt. 

Zwar war mir schon sehr lange Bewusst, dass Test-Ergebnisse kontra-intuitiv sein können, weil sie eben auch Gesunde als krank erkennen können.

Das ist einfach nachzuvollziehen, wenn man einen Covid-Schnelltest anschaut. Die Güte dieser Tests wird fast immer mit zwei Zahlen angegeben: Sensitivität (wie viele der tatsächlich Kranken werden erkannt) und Spezifizität (wie viele der Gesunden werden auch als Gesund erkannt). Nehmen wir an unser Test hat jetzt 90% Sensitivität und 90% Spezifität (fiktive Zahlen für leichteres Rechnen - echte Zahlen kommen später).

Wende ich diesen Test jetzt auf 100 menschen in einem Land an, das Zero-Covid hat. Dann sollten trotzdem 10 Personen als Krank gemeldet werden. Was heißt das jetzt? Logischerweise nix, denn es sind ja alle Gesund.

Gehe ich stattdessen ins Krankenhaus auf die Covid-Station und mache den Test dort mit 100 Personen, dann sollten trotzdem 10 Personen als Gesund gemeldet werden. Was heißt das jetzt? Logischerweise auch nix, die Leute sind ja schon wegen Covid in Behandlung und nicht durch den Test plötzlich gesundet.

Und das ist die wichtige Denk-Änderung die 3Blue1Brown in seinem Video motiviert - man muss beim nachdenken über Testergebnisse einfach immer auch darüber nachdenken wie wahrscheinlich es ist das man tatsächlich Krank ist. ([Bayes läßt grüßen](https://de.wikipedia.org/wiki/Satz_von_Bayes))

Und jetzt kommt der Dreh - alles wird viel einfacher, wenn man von vorne herein nicht denkt dass der Test mir sagt ob ich gesund oder Krank bin, sondern:

> Jeder Test hat ein *Vorhersagekraft*, und dieser Faktor *verändert meine Wahrscheinlichkeit* Krank zu sein.

Es wäre also viel Einfacher, wenn man bei Tests nicht Sensitivität und Spezifizität angibt, sondern daraus die *Vorhersagekraft* des Tests berechnet. Damit kann man dann nämlich plötzlich viel einfacher Denken und verstehen. Und insbesondere ist es viel einfacher  eine 'Vorhersagekraft' nicht mit 'Wahrscheinlichkeit das ich Krank bin' zu verwechseln, was einfach jeder tut der sich mit dem Thema nicht intensiv auseinander setzt.

Und das beste: Wenn man nicht in Prozent, sondern in Verhältnissen rechnet, dann ist das sogar präzise!

Wie kommt man jetzt auf diese *Vorhersagekraft* (auch Bayes Faktor genannt)

Jeder Test ist durch zwei Zahlen (Sensitivität[welcher Anteil der Kranken wird erkannt], Spezifität[welcher Anteil der Gesunden wird als Gesund erkannt]) bzw. durch die Komplemente davon (Falsch-Negativ-Rate, Falsch-Positiv-Rate) bestimmt.

Interessiert man sich jetzt für die Frage was ein Positiver-Test aussagt, kann man aus dem Quotient von Sensitivität geteilt durch Falsch-Positiv-Rate die Vorhersagekraft eines Positiven Tests und aus dem Quotient von Falsch-Negativ-Rate geteilt durch Spezifität die Vorhersagekraft eines Negativen Tests erhalten.

Ein Beispiel: Angenommen ein Test hat Sensitivität 90% und Spezifität 95% (also Falsch-Negativ-Rate 10%, Falsch-Positiv-Rate 5%). Dann ist die Vorhersagekraft eines Positiven Tests 90%/5%=18. Das heißt, wenn ich einen Positiven Test habe, dann weiß ich, dass sich die Wahrscheinlichkeit das ich Krank bin 18 mal vergrößert hat zu dem wie sie vorher war. Und umgekehrt: Ein Negativer Test 10%/95%≈  1/10. In Worten: Ein Negativer Test verkleinert meine Chance krank zu sein um etwa eine Größenordnung.

> 🤯

Rechnet man in Prozent ist das leider nur eine Abschätzung, aber wenn man das ganze in [Chancen](https://de.wikipedia.org/wiki/Chance_(Stochastik)) rechnet wird aus der Abschätzung eine präzise Formel!

> 🤯 🤯

Schauen wir also mal auf ein paar reale Zahlen an.

In Berlin (stand 13. Mai 21, sind nach [Pavels Covid Tabelle](https://pavelmayer.de/covid/risks/)) derzeit einer von 394 Personen Ansteckend.

Für einen Covid-Test den ich gerade da habe [findet sich hier](https://images.vinocentral.de/media/pdf/87/1d/be/BOSON-SARS-CoV-2-Antigen-Schnelltest-Information-Zusammenfassung.pdf) eine Sensitivität ~93,5% und Spezifität ~98%.

Die Vorhersagekraft eines Positiven Tests ist  93,5%/2% ~47. Demnach wächst meine Chance heute ansteckend zu sein von 1/394 um das fast fünfzigfache auf etwas mehr als 1/4.

Die Vorhersagekraft eines negativen Tests ist 6,5%/98%~0,06. Demnach wächst meine Chance Gesund zu sein um fast zwei Größenordnungen von 1/394 auf etwa 6,5/38.612.

Oh wie schön wäre es, wenn das auch überall so kommuniziert würde…
---
categories: miscellaneous, politics
---
pub_date: 2021-05-13
