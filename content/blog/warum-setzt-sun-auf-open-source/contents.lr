
title: Warum setzt Sun auf Open Source?
---
author: Martin Häcker
---
pub_date: 2009-02-11
---
categories: politics, miscellaneous
---
body:

Neulich habe ich einen [spannenden Pocast mit einem Interview von Simon Phipps](http://twit.tv/floss39) gehört. Das Thema: Warum setzt Sun so auf Open Source wie sie es tun?

Das ganze Interview war spannend - vor allem weil Simon neben dem Marketting-Speak von Sun auch tatsächlich etwas interessantes zu seinen Ansichten über die Gesellschaft gesagt hat. Und das finde ich "spot on".

Hier ein paar ausschnitte.

-----
> Why should a company like Sun be at all concerned about open source?

The key thought is anchored in looking at thrends in society. If you look at whats happening in societies around the world, ever since the internet became indemic, there has been a topological shift in the structure of society. Society used to be structured on a hub and spoke basis. With people controlling communications and rare resources at the hub and citizens and employees and consumers at the spokes.

What the pervasive nature of the internet made happen, was that the topology of society gradually changed from hub and spoke to mesh. As that's happened, the way that the business interests are being conducted has gradually migrated from a world of secrecy giving confidence and security to a world with transparency with privacy giving confidence and security.

We have looked at that trend - and we are absolutely convinced that if we are going to be a leading technology company in the 21st century, we have to adapt the company to live in that meshed society and to fit in with the emerging norm of transparency with privacy.


> Do other companies see the world as you do?

I think that it is pretty slow spreading. One of the problems that you have as a business is that you get trapped in what I call the success trap. There's a book from Richard Dawkins called "Climbing Mountain Probable" where he talks about the success trap - how organisms can only evolve to a pinacle of quality. And they can never evolve to a higher pinacle if the route to that higher pinacle if the route to that higher pinacle involves degrading the organism. They can never go down to a valley to reach a higher peak. And it's the same with businesses. A business that is successfull and profitable, cannot afford to become unsuccessfull and unprofitable in order to become better.

Companies have to exploit their fallow periods. IBM did this very successfully by using it's failure to reinvent itself. I believe that that is what Sun has been doing at the beginning of this decade as well. It was using a period of failure and unprofitability to transform itself. Other companies which did not have this blessing of failure (which is a funny word - but still true), will indeed find themselves in a really tough place as we move into the society that is based on a mesh culture and that values transparency with privacy.

You could look at the dilemma that the music industry has got. Still making lots of money, a lot of that money is based on a paranoia around keeping secrets and keeping control. The're going to have a hell of a time transforming into an industry for the networked society. Guess what: We're beginning to see the symptoms of that as they attacked their customers and as they try and protect outdated business models.


...

This is what absolutely fascinates me. I think that what we're seing here is a meta-effect in action. I think that by introducing the web browser and providing a really good reason why the world wanted to get wired, Tim Berners Lee has triggered an epochal change in the structure of society. And that's going to work itself out in all sorts of ways.

...

I was having a conversation with a customer a little bit earlier on, talking to them about the stages that businesses go through as they become open source businesses. I definitely see an evolutionary cycle in becoming an open source business.

> So the first step is to make it free as in beer. No charge.

I think there's a stage before there. The Instinct to keep secrets is so strong in most software companies that the first stage that they go through is the willingness to collaborate with their friends and partners. That's Microsoft shared source for example. Every business that has depended on secret software and secret source goes through that first stage of wanting to open up but retain control and boundaries.

They then go on to a stage where they realize that they can be completely open but under the terms of a license. That's the stage where companies are busy nitpicking over every line in every license and are trying to work out the perfect licenses. It's that stage of maturity that lead to license proliferation at OSI and it was inevitable in my view.

The stage beyond there is where they begin to realize that code is king. And that the license doesn't matter so much as long as the code gets written and gets written fast and well. 

And then there's a stage beyond that, where people begin to realize that community is king and that actually you don't have to write all the code and you don't have to control all the code. It's actually the community where all the benefits that make you successful are gonna come from.

I see businesses gradually moving along that maturity timescale. Going through each of the phases and believing that it's the endpoint. It may be that there's a point on after communities that I haven't seen yet. 

