title: Was ist blooms taxonomie? Die 'wichtigsten' oberen Ebenen
---
author: Martin Häcker
---
categories: miscellaneous
---
pub_date: 2024-09-06
---
body:

![Blooms Taxonomie](../waehrend-dem-lernen-lernen-lernen-blooms-taxonomie/bloom.jpeg)

In [meinem ersten Blog-Post habe ich mich mit Blooms Taxonomie beschäftigt](../waehrend-dem-lernen-lernen-lernen-blooms-taxonomie/). Im [zweiten ging es um die ersten drei Ebenen seines Lernmodells](../waehrend-dem-lernen-lernen-lernen-blooms-taxonomie-ebene-1-3/). Hier geht es jetzt um die mir am wichtigsten ebenen vier und fünf. Und der Vollständigkeit halber auch auch um die sechste.

Und los gehts!

## Die Ebenen von [Blooms Taxonomie](https://de.wikipedia.org/wiki/Blooms_Taxonomie):

### 4. Analyse

Auf dieser Ebene geht es darum, Wissen und Informationen in seine Bestandteile zu zerlegen um Beziehungen und Strukturen zu erkennen und Verstehen. Hier geht es erstmals um kritisches Denken und das erkennen von Mustern.

*Fragen*: "Welche Elemente bilden das Gesamtkonzept?" oder "Wie hängen die Teile zusammen?" und "Was sind die zugrunde liegenden Annahmen?" Diese Fragen helfen uns, tiefer in das Thema einzutauchen und es aus verschiedenen Perspektiven zu betrachten.

*Tätigkeit*: Untersuchen, Zerlegen, Beziehungen erkennen, Vergleichen. Hier geht es darum, das erlernte Wissen zu analysieren, zu hinterfragen und mit schon gelerntem aus anderen Themen zu vergleichen und zu kontrastieren.


Die Fähigkeit, komplexe Informationen zu durchdringen und logische Verbindungen herzustellen.

*Lernziel*: Ich kann komplexe Informationen durchdringen, zerlegen und logische Beziehungen erkennen. Auch zu anderem bisher gelernten. Das Ergebnis dieser Lernstufe ist die Fähigkeit, das Gelernte zu analysieren und zu hinterfragen.

Daher ist diese Ebene und Ihre Fragestellung auch mit der nächsten so wichtig, um langfristig zu behalten was man gelernt hat (verknüpfungen und kontrastierung zu anderem was man schon kann). Außerdem: Wenn man sich von Anfang an mit diesen Fragen beschäftigt, kriegt man in der gleichen Lernzeit Ebene 1-3 quasi umsonst.

*Beispiel*: Ein Softwareentwickler könnte beispielsweise den Code eines Programms analysieren, um Fehler zu finden oder um zu verstehen, wie verschiedene Teile des Codes zusammenarbeiten. In einem Konflikt könnte ich die Argumente analysieren, um Bestandteile, Beziehungen zwischen den Argumenten und die dahinter stehenden Annahmen, deren Logik, und die historische Beziehung der konflikteten Parteien zu bewerten.

### 5. Evaluieren

Jetzt, wo wir das Wissen analysiert und zerlegt haben, geht es darum, dieses Wissen zu bewerten, zu kritisieren und zu priorisieren. Es geht darum, fundierte Urteile zu fällen und Kritik zu üben.

*Fragen*: "Ist diese These Valide?", "Wie effektiv ist diese Methode?", "Welche vor und Nachteile hat dieser Ansatz?", "Welcher andere Ansatz ist aus welchen Gründen besser?" oder "Welche Methode sollte ich priorisieren?" Diese Fragen helfen uns, das Wissen zu bewerten und Entscheidungen zu treffen.

*Lernziel*: Das Ergebnis dieser Lernstufe ist die Fähigkeit, das Gelernte kritisch anhand der für meine Situation relevanten Kriterien zu bewerten und Entscheidungen zu treffen. Es geht darum, das Wissen zu nutzen, um fundierte Entscheidungen zu treffen und Prioritäten zu setzen.

*Tätigkeit*: Führen von Debatten, Entscheidung zwischen Ansätzen, Bewerten von Projekten und Theorien. Hier geht es darum, das erlernte Wissen zu bewerten und zu priorisieren.

*Beispiel*: In einem Projekt oder bei der Softwareentwicklung verschiedene Strategien oder Umsetzungsmöglichkeiten bewerten und entscheiden, welche am besten umgesetzt werden sollte. Oder auch kleiner: Bewertung der Glaubwürdigkeit einer Quelle. Kritisches hinterfragen von Maßnahmen und (politischen) Entscheidungen.

### 6. Erzeugen

Einleitung: Die höchste Ebene ist das Erschaffen, bei dem ein neues oder originelles Werk entwickelt wird. Es geht darum, Wissen und Ideen auf innovative Weise zu kombinieren und neue Konzepte zu entwickeln.

*Fragen*: "Was wäre, wenn...?" oder "Wie könnte ich...?". "Könnte ich [Problem X] vielleicht auch ganz anders angehen?", "Kann ich diese Konzepte vielleicht auch so kombinieren?"

*Lernziel*: Kreativit und Innovativ eigene Ideen in Produkte umsetzen.

*Tätigkeit*: Entwickeln von Projekten oder Modellen. Schreiben von Texten. Entwerfen von Experimenten oder Prototypen.

*Beispiel*: Als Softwareentwickler entwickle ich eine Software. Ich schreibe ein Gedicht. Ich führe eine neue Software ein um ein altes Problem neu besser zu lösen.


## Warum ist mir das so wichtig?

Seit den 1960er Jahren wird Blooms Taxonomie dazu verwendet, um einzuschätzen, wie gut ich und andere ein Thema verstanden haben. Und erst jetzt kriege ich raus, dass es a) existiert, und ich es b) dazu verwenden kann mein eigenes Lernen besser zu verstehen und zu vertiefen? Boah ey, wieso hat mir das in der Schule niemand erklärt? Oder wenn, wieso dann nicht auf eine Weise die ich verstanden habe? Ich fühle mich im Moment so, als wurde mir hier über Jahre etwas wichtiges vorenthalten.

So, das wars. Jetzt wisst Ihr so viel über das Modell und seine Ebenen wie ich auch. [Nächstes mal soll es dann darum gehen wie man mit hilfe von KI sich selbst auf die Ebene des Lernens Katapultieren kann auf der man gerade sein will.](../../10/waehrend-dem-lernen-lernen-lernen-ki-nutzen/)

Bis dahin, bleibt neugierig und probiert es doch mal aus in diesen Kategorien über euer eigenes Lernen nachzudenken.
