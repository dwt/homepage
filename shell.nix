{
  pkgs ? import <nixpkgs> { },
}:
pkgs.mkShell {
  buildInputs = with pkgs; [
    # Add your build inputs here
    pkgs.python313
    pkgs.uv
    pkgs.git
  ];
  env = {
    UV_PYTHON = pkgs.python313;
    UV_DOWNLOAD_PYTHON = "never";
  };
}
