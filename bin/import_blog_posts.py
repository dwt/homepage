#!/usr/bin/env python3

from fluentpy import _, lib, each


def get_latest_posts(path_to_db):
    db_connection = lib.sqlite3.connect(path_to_trac_sqlite)
    db_connection._.row_factory = lib.sqlite3.Row._
    cursor = db_connection.cursor()
    all_current_posts_by_with = cursor.execute(r'''
        with
            latest_version_of(name, version) as (
                select name, max(version) from fullblog_posts group by name
            )
        select f.* from fullblog_posts as f join latest_version_of as l USING(name) where f.version = l.version
    ''').fetchall()
    return all_current_posts_by_with

def trac_wiki_to_markdown_and_attachments(text):
    referenced_images = []
    def format_and_record_image(match):
        referenced_images.append(match.group(1))
        return match.expand(r'![Image](\2)')
    def format_code_block(match):
        languages = { 'text/x-objc': 'objc', 'text/x-elisp': 'lisp'}
        trac_language = match.group(1)
        pygments_language = languages.get(trac_language, trac_language or '')
        code = match.group(2)
        # if trac_language and 'sh' in trac_language:
        #     import pdb; pdb.set_trace()
        return f'```{pygments_language}\n{code}```'
    fixers = [
        (r'\[\[BR\]\]', r'\n\n'),
        (r'\[\[AddComment\]\]', ''), # remove comments
        (r'{{{\s*(.*?)\s*}}}', r'`\1`'), # embedded one line source
        (r'(?sm){{{\s*(?:#!(.+?))?\s+(.*?)}}}', format_code_block), # embedded source
        (r'(?m)\[(https?://\S+?)\s+(.*?)\]', r'[\2](\1)'), # links
        (r'(?m)^(=+)\s*(.*?)\s*=+\s*?$', lambda match: '#' * len(match.group(1)) + ' ' + match.group(2)), # fix headlines
        (r'(?m)^(-{3,})\s*$', lambda match: match.group(1) + '-'), # fix lines
        (r"(?m)('{2,4})(.*?)(\1)", lambda match: len(match.group(1)) * '*' + match.group(2) + len(match.group(1)) * '*'), # italic, bold, bold italic
        (r'(?m)\[\[Image\(((?=https?://).*?)(?:, .*?)?\)\]\]', r'![Image](\1)'), # url image links (filter these out first)
        (r'(?m)\[\[Image\(((?:.*?:\s*)?.+?\s*:\s*(.*?))(?:, .*?)?\)\]\]', format_and_record_image), # image links
        (r'!([A-Z][a-z]+(?:[A-Z][a-z]+)*)', r'\1'),
    ]
    def fix(text, fixer):
        regex, replacement = fixer
        return _(text).sub(regex, replacement)._
    return _(fixers).reduce(fix, text)._, referenced_images

def slugify(text):
    return lib.unicodedata.normalize('NFKD', text).sub(r'ß', 'ss').sub(r'\W', '-').sub(r'-+', '-').sub(r'(?:^[-_]|[-_]$)', '').lower()._

def map_author(author):
    return 'Martin Häcker'

def format_date(date_string):
    return lib.datetime.datetime.fromtimestamp(date_string).strftime('%Y-%m-%d')._

def map_categories(categories_string):
    mappings = dict(
        software='code',
        projekt='project',
        links='miscellaneous',
        lucid='miscellaneous',
        podcast='miscellaneous',
        review='miscellaneous',
        policitcs='politics',
        politik='politics',
    )
    return _(categories_string).split(r',?\s+').map(lambda each: mappings.get(each, each)).call(set).join(', ')._

def lektor_format(post):
    template = lib.textwrap.dedent("""
    title: {title}
    ---
    author: {author}
    ---
    pub_date: {publish_time}
    ---
    categories: {categories}
    ---
    body:
    
    {body}
    
    """)._
    post_dict = dict(post)
    post_dict['title'], ignored = trac_wiki_to_markdown_and_attachments(post_dict['title'])
    post_dict['categories'] = map_categories(post_dict['categories'])
    post_dict['publish_time'] = format_date(post_dict['publish_time'])
    post_dict['author'] = map_author(post_dict['author'])
    post_dict['body'], attachments = trac_wiki_to_markdown_and_attachments(post_dict['body'])
    return template.format(**post_dict), attachments

def old_path(attachment):
    extracted_path = _(attachment).sub(r'[\n]*', '').sub(r':', '/')._
    if extracted_path.split('/')[0] not in ('blog', 'wiki'):
        extracted_path = 'wiki/' + extracted_path
    actual_path = lib.os.path.join(attachments_dir, extracted_path)._
    if not lib.os.path.exists(actual_path)._:
        # see if it is somewhere else
        actual_path = lib.subprocess.check_output(['find', attachments_dir, '-name', lib.os.path.basename(extracted_path).sub(' ', '%20')._]).decode().strip()._
    return actual_path

def new_path(post_name, attachment):
    return lib.os.path.join(path_to_output_dir, post_name, _(attachment).split(r':')[-1]._)._

def urls_from_post(post):
    old_url = '/trac/blog/' + _(post['name']).sub(':', '/')._
    date = lib.datetime.datetime.fromtimestamp(post['publish_time'])._
    new_url = '/blog/' + f'{date.year}/{date.month}/' + slugify(post['title']) + '/'
    return (old_url, new_url)


path_to_trac_sqlite = lib.sys.argv.get(1, "/Users/dwt/Code/Archiv/trac_blog/web/default/userdata/trac/db/trac.db")._
path_to_output_dir = lib.sys.argv.get(2, '/Users/dwt/Code/Projekte/homepage/content/blog/')._
attachments_dir = '/Users/dwt/Code/Archiv/trac_blog/web/default/userdata/trac/attachments/'

with open(lib.os.path.join(lib.os.path.dirname(lib.os.path.dirname(lib.os.path.dirname(path_to_output_dir)._)._)._, 'url_mapping.csv')._, 'w') as mappings_file:
    mappings_file.write('old_blog_post_url, new_blog_post_url\n')
    
    for post in get_latest_posts(path_to_trac_sqlite):
        post_name = slugify(post['title'])
        post_folder_path = path_to_output_dir + post_name
        lib.os.makedirs(post_folder_path, exist_ok=True)
        with open(lib.os.path.join(post_folder_path, 'contents.lr')._, 'w') as content_file:
            lektor_post, attachments = lektor_format(post)
            content_file.write(lektor_post)
            old_url, new_url = urls_from_post(post)
            mappings_file.write(f'{old_url}, {new_url}\n')
            for attachment in attachments:
                lib.shutil.copy(old_path(attachment), new_path(post_name, attachment))




# for  match in referenced_images:
#     print(old_path(match))
#     # lib.shutil.move(old_path(match), new_path(match))
