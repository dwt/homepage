# Install

https://pipenv.pypa.io/en/stable/

    $ pipenv install --dev

# Update packages

Check what is outdated

    $ pipenv update --outdated

Update everything

    $ pipenv update

# Start lokal server for writing

    $ make serve

# Build and deploy site

    $ make deploy
