title: Yeehaw! Endlich wieder ein Blog!
---
author: Martin Häcker
---
body:

Lange hat es gedauert - Ahem.

In den letzten Monaten habe ich mich mit [Lektor](https://getlektor.com) auseinandergesetzt. Mit ein Paar Plugins ([CreativeCommons](https://github.com/humrochagf/lektor-creative-commons) und [Markdown Highlighter](https://github.com/lektor/lektor-markdown-highlighter)) kann man viel Spaß beim Blog-Post schreiben haben.

Der große Vorteil von Lektor ist, dass man eine komplett statische Seite damit rendert. Man kann die Seite danach also über ein CDN ausliefern - 'Internet Scale' ist also gar kein Problem…

Solche Systeme gibt es natürlich viele - aber mit Lektor kriegt man zusätzlich noch ein CMS (das Lokal läuft) - oder auch hinter `.htaccess` geschützt laufen kann, so dass man keine Sorgen haben muss, dass einem über so eine Webseite der Server aufgemacht wird.

Aber Lektor überzeugt auch mit einer moderat einfachen [API](https://www.getlektor.com/docs/api/), selbst plugins dafür zu schreiben ist einfach und macht Spaß. Fast wie es bei [Track](https://trac.edgewall.org) ganz am Anfang auch einmal war. ☺️

Ich freue mich darüber dass ich jetzt eine voll-responsive Seite haben, die mit HTML und CSS wirklich auskommt - no JS needed. (Ich werde aber vielleicht in der Zukunft für Experimente auch JS in der Seite verwenden). Für jetzt ist es erst mal das Experiment wie lange ich ohne JS auskomme.

Da wäre zum Beispiel das Menü - das auf Mobil mit Animation schön aufklappt. Hierzu habe ich verschiedene Techniken kombiniert.

1. Die Verhältnisse der ganzen Abmessungen werden über [CSS-Variablen](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_variables) und [CSS-Calc](https://developer.mozilla.org/de/docs/Web/CSS/calc) einmal global gesetzt. Das geht zwar noch nicht vollständig, da man CSS Variablen [nicht in Media Queries einsetzen kann](https://stackoverflow.com/questions/40722882/css-native-variables-not-working-in-media-queries#40723269), aber immerhin. Einzig dass man Stylesheets noch nicht 'nested' aufschreiben kann stört.

1. Die Animation beim klick auf das [Hamburger Menü](https://de.wikipedia.org/wiki/Hamburger-Menü-Icon) sowie das Öffnen des Menüs kommt komplett ohne JS aus. Damit das geht braucht es ein DOM Element, dass den State 'ein oder ausgeblendet' hält, gleichzeitig via CSS abgefragt werden kann sowie diesen State durch Klick auf ein anderes Element ändert. Well, dafür funktioniert lustiger Weise das `<input type=checkbox id=menu-state>` Element. Das wechselt den `checked`-Zustand, was mann dankenswerter Weise in CSS mittels `:checked` herausfinden kann. Der Clou ist aber, dass man irgendwo anders im Dokument ein `<label for=menu-state>` haben kann, auf das man Klicken kann um diesen State zu wechseln. Und `<label>` kann man im Gegensatz zu `<input type=checkbox>` in allen Browsern vernünftig stylen. Und man kann mittels [CSS-Animationen](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations) alles animieren. Fuck yeah!
---
categories: miscellaneous, project, code
---
pub_date: 2018-07-16
